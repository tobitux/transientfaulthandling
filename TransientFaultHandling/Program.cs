﻿using System;
using Polly;

namespace TransientFaultHandling
{
    class Program
    {
        static void Main(string[] args)
        {
           var policy = Policy.Handle<Exception>().Retry(3, (exception, retryCount, context) =>
            {
                Console.WriteLine($"Message: {exception.Message} retryCount: {retryCount}");
            });

            var myObject = new SomeClass();

            try
            {
                policy.Execute(
                    () => myObject.SometimesThrowsAException());
                Console.WriteLine("Successfully called DoSomething");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            Console.ReadLine();
        }
    }
}
