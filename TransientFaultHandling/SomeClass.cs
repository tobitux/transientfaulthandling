﻿using System;

namespace TransientFaultHandling
{
    class SomeClass
    {
        private readonly Random random;

        public SomeClass()
        {
            random = new Random();
        }

        public void SometimesThrowsAException()
        {
            var i= random.Next(0, 100);
            if (i % 2 == 0)
                throw new Exception("Ein Fehler ist aufgetreten.");
        }
    }
}
